from django.urls import path

from gear.views import (
    GearCreateView, 
    GearListView,
    GearUpdateView, 
)

urlpatterns = [ 

    path("<int:riverlog_pk>/create/", GearCreateView.as_view (), name="create_gear"),
    path("<int:pk>/checkoff/", GearUpdateView.as_view(), name="checkoff_gear"),
    path('gear/', GearListView.as_view(), name="list_gear"),
]

