from django.db import models

# Create your models here.

class Gear(models.Model):
    flip = models.BooleanField(default=False)
    swim = models.BooleanField(default=False)
    lost_gear = models.BooleanField(default=False)
    booty_beer = models.BooleanField(default=False)
    riverlog = models.ForeignKey("riverlog.Riverlog", related_name="gear", on_delete=models.CASCADE)


