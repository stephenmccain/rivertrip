from django.contrib import admin

# Register your models here.
from gear.models import Gear

class GearAdmin(admin.ModelAdmin):
    pass


admin.site.register(Gear, GearAdmin)