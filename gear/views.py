from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from gear.models import Gear
from riverlog.models import Riverlog
# Create your views here.
class GearListView(LoginRequiredMixin, ListView):
    model = Gear
    template_name = "gear/list.html"
    context_object_name = "gear_list"

class GearCreateView(LoginRequiredMixin, CreateView):
    model = Gear
    template_name = "gear/create.html"
    fields = ["flip", "swim", "lost_gear", "booty_beer"]

    def get_success_url(self):
        return reverse_lazy("detail_riverlog", args=[self.object.riverlog.id])

    def form_valid(self, form):
        item = form.save(commit=False)
        item.members = self.request.user
        riverlog = Riverlog.objects.get(pk=self.kwargs['riverlog_pk'])
        item.riverlog = riverlog
        item.save()
        return redirect("detail_riverlog", pk=item.riverlog.id)

class GearUpdateView(LoginRequiredMixin, UpdateView):
    model = Gear
    template_name = "gear/update.html"
    fields = ["flip", "swim", "lost_gear", "booty_beer"]


    def get_success_url(self):
        return reverse_lazy("detail_riverlog", args=[self.object.riverlog.id])

    def form_valid(self, form):
        item = form.save(commit=False)
        item.members = self.request.user
        item.save()
        return redirect("detail_riverlog", pk=item.riverlog.id)