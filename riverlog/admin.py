from django.contrib import admin

# Register your models here.
from riverlog.models import Riverlog

class RiverlogAdmin(admin.ModelAdmin):
    pass


admin.site.register(Riverlog, RiverlogAdmin)
