from django.apps import AppConfig


class RiverlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'riverlog'
