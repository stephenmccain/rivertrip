from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin



# Create your views here.
from riverlog.models import Riverlog
def shopping(request):
    return render(request, "riverlog/shopping.html", context={})

def gearlist(request):
    return render(request, "riverlog/gear_pdf.html", context={})

def about(request):
    return render(request, "riverlog/about.html", context={})

class ResourcesListView(ListView):
    model = Riverlog
    template_name = 'riverlog/resources.html'

 

class RiverlogsListView(LoginRequiredMixin, ListView):
    model = Riverlog
    template_name = 'riverlog/list.html'
    context_object_name = "list_riverlog"

    def get_queryset(self):
        return Riverlog.objects.filter(members=self.request.user)




class RiverlogsDetailView(LoginRequiredMixin, DetailView):
    model = Riverlog
    template_name = 'riverlog/detail.html'
 
    def get_success_url(self):
        return reverse_lazy("list_riverlog")

class RiverlogsCreateView(LoginRequiredMixin, CreateView):
    model = Riverlog
    template_name = "riverlog/create.html"
    fields = ['name', 'location', 'put_in_date', 'take_out_date', 'description', 'cfs', 'feet',]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.members = self.request.user
        item.save()
        return redirect("list_riverlog")

    def get_success_url(self):
        return reverse_lazy("list_riverlog")



class RiverlogsUpdateView(LoginRequiredMixin, UpdateView):
    model = Riverlog
    template_name = "riverlog/update.html"
    context_object_name = "update_riverlog"
    fields = ['name', 'location', 'put_in_date', 'take_out_date', 'description', 'cfs', 'feet',]

    def get_success_url(self):
        return reverse_lazy("detail_riverlog", args=[self.object.id])

class RiverlogsDeleteView(LoginRequiredMixin, DeleteView):
    model = Riverlog
    template_name = "riverlog/delete.html"
    success_url = reverse_lazy("list_riverlog")