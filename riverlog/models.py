from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

ALL_STATES= (("Alabama","Alabama"),("Alaska","Alaska"),("Arizona","Arizona"),("Arkansas","Arkansas"),
("California","California"),("Colorado","Colorado"),("Connecticut","Connecticut"),("Delaware","Delaware"),
("Florida","Florida"),("Georgia","Georgia"),("Hawaii","Hawaii"),("Idaho","Idaho"),("Illinois","Illinois"),
("Indiana","Indiana"),("Iowa","Iowa"),("Kansas","Kansas"),("Kentucky","Kentucky"),("Louisiana","Louisiana"),
("Maine","Maine"),("Maryland","Maryland"),("Massachusetts","Massachusetts"),("Michigan","Michigan"),
("Minnesota","Minnesota"),("Mississippi","Mississippi"),("Missouri","Missouri"),("Montana","Montana"),
("Nebraska","Nebraska"),("Nevada","Nevada"),("New Hampshire","New Hampshire"),("New Jersey","New Jersey"),
("New Mexico","New Mexico"),("New York","New York"),("North Carolina","North Carolina"),("North Dakota","North Dakota"),
("Ohio","Ohio"),("Oklahoma","Oklahoma"),("Oregon","Oregon"),("Pennsylvania","Pennsylvania"),
("Rhode Island","Rhode Island"),("South Carolina","South Carolina"),("South Dakota","South Dakota"),
("Tennessee","Tennessee"),("Texas","Texas"),("Utah","Utah"),("Vermont","Vermont"),("Virginia","Virginia"),
("Washington","Washington"),("West Virginia","West Virginia"),("Wisconsin","Wisconsin"),("Wyoming","Wyoming"))


# Create your models here.
class Riverlog(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=15, choices=ALL_STATES, default="Alabama")
    put_in_date = models.DateTimeField()
    take_out_date = models.DateTimeField(blank=True, null=True)
    description = models.TextField(max_length=100)
    cfs = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=0)
    feet = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=2)
    members = models.ForeignKey(
        USER_MODEL, related_name="riverlogs", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

