from django.urls import path

from riverlog.views import (
    RiverlogsListView, 
    RiverlogsDetailView, 
    RiverlogsCreateView, 
    RiverlogsUpdateView, 
    RiverlogsDeleteView, 
    ResourcesListView,
    shopping,
    gearlist,
    about,
)

urlpatterns = [
    path('', RiverlogsListView.as_view(), name="list_riverlog"), 
    path("<int:pk>/", RiverlogsDetailView.as_view(), name='detail_riverlog'),
    path("create/", RiverlogsCreateView.as_view(), name="create_riverlog"),
    path("<int:pk>/update/", RiverlogsUpdateView.as_view(), name="update_riverlog"),
    path("<int:pk>/delete/", RiverlogsDeleteView.as_view(), name ="delete_riverlog"),
    path('listview/', ResourcesListView.as_view(), name='resources_riverlog'),
    path('shopping/', shopping, name='shopping'),
    path('gear_pdf/', gearlist, name='gear_pdf'),
    path('about/', about, name="about"),

]